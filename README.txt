Internet Blackout NZ is an event in protest of changes to copyright laws in NZ that require ISPs disconnect any user accused of copyright infringement, with or without proof.  During the week starting February 15, many Twitter and Facebook users blacked out their avatars in a joint protest against these changes.  On February 23, many websites are blacking out their entire websites for a day.

This module blacks out a Drupal website with a link to creativefreedom.org.nz/forum/topic.php?post=473, where more information about the issue and event can be found.  Some pages are not blacked out, so that the module can be disabled; user, user/*, admin and admin/*.  The list of pages NOT blacked out is configurable (See below).

The blackout page, link, title, CSS and content can be changed with regular theming techniques.  The different methods in order of complexity and amount of control available:
  1. Implement a preprocess function in your theme;  THEMENAME_preprocess_internetblackoutnz_blackout(&$vars).  Modify the variables in $vars and don't return anything.
  2. Override the CSS file/s in your theme;  THEMENAME/internetblackoutnz.css.  Use internetblackoutnz/internetblackoutnz.css as a starting point.
  3. Add additional CSS files;  Implement a preprocess function (option 1 above).  Add your stylesheet to $vars['stylesheets'] then $vars['styles'] = drupal_get_css($vars['stylesheets'])
  4. Override the template file in your theme;  THEMENAME/internetblackoutnz.tpl.php.  Use internetblackoutnz/internetblackoutnz.tpl.php as a starting point.

If you don't want certain pages to be blacked out, add one or both of the the following lines to your settings.php file:
<?php
// Don't blackout pages for which have any of the following as the first argument (the bit before the first '/' slash)
$conf['internetblackoutnz_arg(0)_exceptions'] = array('user');

// Don't blackout the following Drupal system paths.
$conf['internetblackoutnz_path_exceptions'] = array('admin/build/modules');
?>
