<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title><?php print $page_title; ?></title>
    <?php print $styles; ?>
  </head>
  <body>
    <div class="whats-this">
      <h2 class="title"><?php print $title; ?></h2>
      <div class="content"><?php print $content; ?></div>
    </div>
  </body>
</html>
